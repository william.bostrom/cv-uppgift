# Skiss av Webb och Mobilanpassat CV

Detta är en skiss av ett mobilanpassat CV som jag har utvecklat efter en mall. Nedan finner du information om projektet och dess funktioner.

## Beskrivning

Detta projekt är en initial skiss av ett CV som är utformad för att vara webb och mobilanpassat. Syftet med detta projekt är att skapa en grundläggande layout och design för ett CV som ser bra ut och fungerar smidigt på mobila enheter.

## Funktioner

- **Mobilanpassad design:** CV-skissen är optimerad för att se bra ut på små skärmar.

- **Grundläggande innehåll:** Innehållet i skissen inkluderar de viktigaste elementen som vanligtvis finns i ett CV, såsom personlig information, utbildning, arbetslivserfarenhet och färdigheter.

- **Enkel layout:** Layouten är utformad för att vara enkel och lätt att navigera, både på små och stora skärmar.

## Installation

Detta projekt är en skiss och kräver ingen särskild installation. Du kan öppna `index.html` i din webbläsare för att se skissen.

## Användning

För att använda skissen, öppna `index.html` i din webbläsare och navigera genom sidan för att se designen och layouten. Du kan också anpassa innehållet och stilen efter dina egna behov.
